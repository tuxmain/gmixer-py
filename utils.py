#!/usr/bin/env python3
"""
	CopyLeft 2019 Pascal Engélibert <tuxmain@zettascript.org>
	This file is part of ĞMixer-py.

	ĞMixer-py is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ĞMixer-py is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with ĞMixer-py.  If not, see <https://www.gnu.org/licenses/>.
"""

import sys, os, re, socket, time, secrets, hashlib, base64, asyncio, subprocess
import socks
import ubjson
import libnacl.sign
import plyvel
from duniterpy.key import SigningKey, PublicKey
import duniterpy.api.bma as bma
from duniterpy.api.client import Client
from duniterpy.api.errors import DuniterError
import silkaj.money, silkaj.tx

VERSION = "0.2.0"

#-------- DATA

# Decode binary int (Big Endian)
def bin_to_int(b:bytes) -> int:
	return sum([b[i]*256**i for i in range(len(b))])

# Encode int into bin (Big Endian)
# n: number ; b: bytes
def int_to_bin(n:int, b:int=4) -> bytes:
	return bytes([(n>>(i*8))&255 for i in range(b)])

def gen_keys() -> (str, str):
	return secrets.token_urlsafe(), secrets.token_urlsafe()

def gen_comment(seeds:[bytes, bytes, bytes]) -> str:
	return base64.urlsafe_b64encode(hashlib.sha512(b"".join(seeds)).digest()).decode()

#-------- NETWORK

RECBUF = 1024

p_clen = re.compile("\r?\ncontent-length: *(\d+)\r?\n?", re.IGNORECASE)

def sdata(host:(str, int), mode:str, url:str="/", data:bytes=b"", uagent:str="GMixer-py", proxy:(str, int)=None, proxy_onion_only:bool=False) -> (bytes, bytes):
	tsocket = socket.socket
	if proxy and (not proxy_onion_only or re.match("^.+\.onion$", host[0])):
		socks.set_default_proxy(socks.PROXY_TYPE_SOCKS5, proxy[0], proxy[1])
		tsocket = socks.socksocket
	if ":" in host[0]: # IPv6
		sock = tsocket(socket.AF_INET6, socket.SOCK_STREAM)
	else: # IPv4
		sock = tsocket(socket.AF_INET, socket.SOCK_STREAM)
	sock.settimeout(5)
	sock.connect(host)
	sock.settimeout(None)
	
	raw = (mode+" "+url+" HTTP/1.1\r\nHost: "+host[0]+":"+str(host[1])+"\r\nUser-Agent: "+uagent+"\r\nAccept: */*\r\nContent-Length: "+str(len(data))+"\r\n\r\n").encode()+data
	sock.sendall(raw)
	
	paquet = b""
	header = b""
	content = b""
	content_len = 0
	resp = {}
	lf = 0
	while True:
		raw = sock.recv(RECBUF)
		if raw:
			paquet += raw
			if lf >= 0:
				for c in raw:
					if c == 10:# LF
						lf += 1
					elif c != 13:# CR
						lf = 0
					if lf > 1:
						parts = paquet.split(b"\r\n\r\n")
						header = parts[0]
						content = parts[1]
						try:
							content_len = int(p_clen.search(header.decode()).group(1))
						except AttributeError:
							content_len = 0
						break
				if lf > 1:
					break
		else:
			break
	while len(content) < content_len:
		raw = sock.recv(RECBUF)
		paquet += raw
		content += raw
	
	return header, content

#-------- CONSOLE

LOG_TRACE = 1
LOG_INFO = 2
LOG_WARN = 4
LOG_ERROR = 8
LOGMSG_TYPES = {LOG_INFO:"\033[96minfo\033[0m", LOG_TRACE:"\033[39mtrace\033[0m", LOG_WARN:"\033[93mwarn\033[0m", LOG_ERROR:"\033[91merror\033[0m"}
VERBOSITY = LOG_INFO | LOG_WARN | LOG_ERROR

def logprint(msg:str, msgtype:int):
	if msgtype & VERBOSITY:
		print(time.strftime("%Y-%m-%d %H:%M:%S")+" ["+LOGMSG_TYPES[msgtype]+"] "+msg)

def getargv(arg:str, default:str="", n:int=1, args:list=sys.argv) -> str:
	if arg in args and len(args) > args.index(arg)+n:
		return args[args.index(arg)+n]
	else:
		return default

def run_async(coro):
	try:
		return asyncio.get_event_loop().run_until_complete(coro)
	except RuntimeError:
		return asyncio.new_event_loop().run_until_complete(coro)

#-------- ĞMixer

def gen_idty_sig(keys, idty_keys):
	doc = {
		"doctype": "gmixer/idtysig",
		"docver": "1",
		"pubkey": keys.pubkey if isinstance(keys, SigningKey) else keys.base58(),
		"sigtime": int(time.time())
	}
	return idty_keys.sign(ubjson.dumpb(doc)), doc

def default_checktime(conf, data):
	t = time.time()
	return data["sigtime"] < t and data["sigtime"] + conf["server"]["idty_sig_age_max"] > t

def self_checktime(conf, data):
	t = time.time()
	return data["sigtime"] > conf["idty"]["sigtime"] and data["sigtime"] < t and data["sigtime"] + conf["server"]["idty_sig_age_max"] > t

def verify_idty_sig(conf, raw, idty_pubkey, peer_pubkey, checktime=default_checktime):
	try:
		raw = libnacl.sign.Verifier(PublicKey(idty_pubkey).hex_pk()).verify(raw)
		data = ubjson.loadb(raw)
		assert data["doctype"] == "gmixer/idtysig" , "Bad doctype"
		assert data["docver"] == "1" , "Bad docver"
		assert data["pubkey"] == peer_pubkey , "Bad pubkey"
		assert checktime(conf, data) , "Bad sigtime"
	except (ValueError, IndexError, ubjson.decoder.DecoderException, AssertionError) as e:
		logprint("Bad idty sig: "+str(e), LOG_TRACE)
		return None
	return data

class Peer:
	VERSION = "2"
	def __init__(self, conf:dict, raw:bytes, do_check_idty:bool=True):
		self.rectime = time.time()
		self.raw = raw
		try:
			data = ubjson.loadb(raw)
		except ubjson.decoder.DecoderException:
			raise AssertionError("Bad wrapper encoding")
		
		assert "pubkey" in data and "raw" in data , "Missing values in wrapper"
		pubkey = data["pubkey"]
		try:
			raw = libnacl.sign.Verifier(PublicKey(pubkey).hex_pk()).verify(data["raw"])
		except ValueError:
			raise AssertionError("Bad signature")
		try:
			data = ubjson.loadb(raw)
		except ubjson.decoder.DecoderException:
			raise AssertionError("Bad data encoding")
		
		assert "doctype" in data and "docver" in data and "currency" in data and "pubkey" in data and "sigtime" in data and "host" in data and "idty" in data and "idtysig" in data and "peers" in data,\
			"Missing values in data"
		
		assert data["doctype"] == "gmixer/peer" , "Bad doctype"
		assert data["docver"] == Peer.VERSION , "Bad docver"
		assert data["currency"] == conf["currency"] , "Different currency"
		assert data["pubkey"] == pubkey , "Different pubkey"
		assert isinstance(data["sigtime"], (int, float)) , "Bad sigtime"
		assert data["sigtime"] < self.rectime , "Futuristic sigtime"
		assert isinstance(data["peers"], list) , "Bad peer list"
		for p in data["peers"]:
			assert isinstance(p, dict) and "up_in" in p and "up_out" in p and p["up_in"] in (None, False, True) and p["up_out"] in (None, False, True),\
				"Bad peer list value"
		
		if do_check_idty and conf["idty"]["needed"]:
			assert run_async(check_idty(conf["client"]["bma_hosts"], data["idty"])) , "Idty is not member"
			assert verify_idty_sig(conf, data["idtysig"], data["idty"], pubkey) , "Bad idty sig"
		
		self.doctype = data["doctype"]
		self.docver = data["docver"]
		self.pubkey = data["pubkey"]
		self.sigtime = data["sigtime"]
		self.host = tuple(data["host"]) # socket cannot manage lists
		self.idty = data["idty"]
		self.idtysig = data["idtysig"]
		self.peers = data["peers"]# TODO check size & useless values
		
		self.hash = hashlib.sha512((self.pubkey+"@"+self.host[0]+":"+str(self.host[1])).encode()).digest()
		self.keys = PublicKey(self.pubkey)
		
		self.up_in = None # can reach local node
		self.up_out = None # is reachable by local node
	
	def to_human_str(self, short=True):
		return (self.pubkey[:8] if short else self.pubkey)+"@"+self.host[0]+":"+str(self.host[1])
	
	def generate(conf:dict, keys:SigningKey, peers:list) -> bytes:
		data = {
			"doctype": "gmixer/peer",
			"docver": Peer.VERSION,
			"currency": conf["currency"],
			"pubkey": keys.pubkey,
			"sigtime": time.time(),
			"host": [conf["server"]["public_host"], conf["server"]["public_port"]],
			"idty": conf["idty"]["pubkey"],
			"idtysig": bytes.fromhex(conf["idty"]["sig"]),
			"peers": [{"hash": peers[p].hash, "up_in": peers[p].up_in, "up_out": peers[p].up_out} for p in peers]
		}
		raw = keys.sign(ubjson.dumpb(data))
		data = {
			"pubkey": keys.pubkey,
			"raw": raw
		}
		raw = ubjson.dumpb(data)
		try:
			return Peer(conf, raw, do_check_idty=False)
		except AssertionError as e:
			logprint("Gen peer: "+str(e), LOG_ERROR)
			return None

def load_peers(conf:dict, db_peers:plyvel.DB, peers:dict):
	to_remove = []
	for peer_hash, data in db_peers:
		try:
			peer = Peer(conf, data)
		except Exception as e:
			logprint("Importing peer: "+str(e), LOG_WARN)
			to_remove.append(peer_hash)
			continue
		peers[peer.hash] = peer
	
	for peer_hash in to_remove:
		db_peers.delete(peer_hash)

def save_peers(db_peers:plyvel.DB, peers:dict):
	for peer in peers:
		db_peers.put(peers[peer].hash, peers[peer].raw)

async def bma_client(bma_endpoints:list):
	client = None
	downs = 0
	for bma_endpoint in bma_endpoints:
		client = Client(bma_endpoint)
		try:
			await client(bma.node.summary)
			logprint("BMA up: "+bma_endpoint, LOG_TRACE)
			break
		except:
			logprint("BMA down: "+bma_endpoint, LOG_WARN)
			await client.close()
			client = None
			downs += 1
	
	# Put down endpoints at the end
	for i in range(downs):
		bma_endpoints.append(bma_endpoints.pop(0))
	return client

async def check_idty(bma_endpoints:list, pubkey:str):
	client = await bma_client(bma_endpoints)
	try:
		result = await client(bma.wot.requirements, pubkey)
	except DuniterError:
		result = {}
	finally:
		await client.close()
	return "identities" in result and len(result["identities"]) > 0 and result["identities"][0]["pubkey"] == pubkey and result["identities"][0]["membershipExpiresIn"] > 0

#async def send_transaction(sender_keys:SigningKey, receiver_pubkey:str, amount:int, comment:str):
	#sender_amount = silkaj.money.get_amount_from_pubkey(sender_keys.pubkey)[0]
	#assert sender_amount >= amount, "not enough money"
	
	#await silkaj.tx.handle_intermediaries_transactions(sender_keys, sender_keys.pubkey, amount, [receiver_pubkey], comment)

def send_transaction(authfile:str, receiver_pubkey:str, amount:int, comment:str):
	subprocess.Popen(["silkaj", "--auth-file", "--file", authfile, "tx", "--output", receiver_pubkey, "--amount", str(amount/100), "--comment", comment, "-y"])
