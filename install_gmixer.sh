#!/usr/bin/env sh
# gmixer-py installation : installation de gmixer-py : mixeur de monnaie libre
# License : Creative Commons http://creativecommons.org/licenses/by-nd/4.0/deed.fr
# Project : https://git.duniter.org/tuxmain/gmixer-py
# contact mail : <pytlin@protonmail.com>


VERSION="2019.05.06"

#set -e
#set -x

# VARIABLES
blank="\033[1;37m"
grey="\033[0;37m"
pink="\033[0;35m"
red="\033[1;31m"
green="\033[1;32m"
yellow="\033[1;33m"
blue="\033[1;34m"
rescolor="\033[0m"

noError="false"


# Start of script
printf "\n%b$green\n"
printf "######################################################################\n"
printf "#                                                                    #\n"
printf "#                    Installation script GMIXER                      #\n"
printf "#                                                                    #\n"
printf "#                     Tested on Debian 9.8 x64                       #\n"
printf "#                           by @pytlin                               #\n"
printf "#                                                                    #\n"
printf "#     For any bug please send mail to pytlin@protonmail.com          #\n"
printf "#                                OR                                  #\n"
printf "# Create an issue here :  https://git.duniter.org/tuxmain/gmixer-py  #\n"
printf "#                                                                    #\n"
printf "######################################################################\n"
printf %b "\t\t\t$VERSION\n"
printf %b "$rescolor\n"
sleep 3


if test "$(id -u)" = "0"
then
    printf "\n\n%b\t\tDon't Run this script as root.%b\n\n" "${yellow}" "$rescolor" 
	exit 1
fi




if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
#elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
 #   OS=$(lsb_release -si)
  #  VER=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
elif [ -f /etc/SuSe-release ]; then
    # Older SuSE/etc.
    ...
elif [ -f /etc/redhat-release ]; then
    # Older Red Hat, CentOS, etc.
    ...
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
fi


#---------------------------functions-------------------------------------------------------------

helpFunction ()
{
   printf ""
   echo "Usage: $0 -t [1,2,3] -v -h -g -p <port>\n"
   printf "\t-t This parameter enable to create onion node\n"
   printf "\t\t 1 => mixer node without tor (default value)\n"
   printf "\t\t 2 => mixer node connected to tor\n"
   printf "\t\t 3 => mixer node into tor network : hidden service\n"
   printf "\t-g This parameter permits to generate randomly the salt and password\n"
   #printf "\t-d This parameter permits to launch gmixer in daemon\n"
   printf "\t-v This parameter activate verbose mode\n"
   printf "\t-p This parameter permits to specify a port (default port is 10951)\n"
   printf "\t-h This parameter print out the help\n"
   exit 1 # Exit script after printing help
}

helpFunctionWithoutExit ()
{
   printf ""
   echo "Usage: $0 -t [1,2,3] -v -h -g -p <port>\n"
   printf "\t-t This parameter enable to create onion node\n"
   printf "\t\t 1 => mixer node without tor (default value)\n"
   printf "\t\t 2 => mixer node connected to tor\n"
   printf "\t\t 3 => mixer node into tor network : hidden service\n"
   printf "\t-g This parameter permits to generate randomly the salt and password\n"
   #printf "\t-d This parameter permits to launch gmixer in daemon\n"
   printf "\t-v This parameter activate verbose mode\n"
   printf "\t-p This parameter permits to specify a port (default port is 10951)\n"
   printf "\t-h This parameter print out the help\n"

}




# Read secret string #read -s is not compliant POSIX
readSecret ()
{
    # Disable echo.
    stty -echo

    # Set up trap to ensure echo is enabled before exiting if the script
    # is terminated while echo is disabled.
    trap 'stty echo' EXIT

    # Read secret.
    read "$@"

    # Enable echo.
    stty echo
    trap - EXIT

    # Print a newline because the newline entered by the user after
    # entering the passcode is not echoed. This ensures that the
    # next line of output begins at a new line.
    echo
}



#retrieve public ip
getPublicIp () {
	
	curl -s ifconfig.me 

}

#retrieve project gmixer from gitlab
#todo : test if folder exists
getGmixerProject () {
	noError="false"
	cd $HOME
	rm -rf $HOME/gmixer-py
	if [ ! -d $HOME/gmixer-py ]; then 
		git clone https://git.duniter.org/tuxmain/gmixer-py.git --quiet
		checkCommand
	fi
}

#generate configuration folder (default folder : $HOME/.gmixer)
generateConfFolder () {
	noError="false"
	if [ ! -d "$HOME/.gmixer" ]; then 
		if [ "$OS" = "Debian GNU/Linux" ] || [ "$OS" = "Ubuntu" ] || [ "$OS" = "Raspbian GNU/Linux" ]; then
			$(which python3) $HOME/gmixer-py/server.py -i 2> /dev/null
			checkCommand
			if test $noError = "false"
			then
				$(which python3.7) $HOME/gmixer-py/server.py -i 2> /dev/null
				checkCommand
			fi
		elif [ "$OS" = "CentOS Linux" ]; then
			$(which python3) $HOME/gmixer-py/server.py -i 2> /dev/null
			checkCommand
		else
			$(which python3) $HOME/gmixer-py/server.py -i 2> /dev/null
			checkCommand
		fi
	else
		noError="true"
	fi
}

printOkKo() {
	
	if test "$noError" = "true"
	then
		printf "%bOK%b\n" "${green}" "${rescolor}"
	else 
		printf "%bKO%b\n" "${red}" "${rescolor}"
		printf "%bThe installation is aborted due to error%b\n" "${red}" "${rescolor}"
		exit 1
	fi

}

checkCommand() {

	if [ $? -eq 0 ]; then
		noError="true"
	else 
		noError="false"
	fi

}


#update system with package manager : apt for debian derivative system and yum for centos derivated system
updateSystem () {
	noError="false"
	if [ "$OS" = "Debian GNU/Linux" ] || [ "$OS" = "Ubuntu" ] || [ "$OS" = "Raspbian GNU/Linux" ]; then
		sudo apt-get update -y   && sudo apt-get upgrade -y 
		checkCommand
	elif [ "$OS" = "CentOS Linux" ]; then
		sudo yum update -y   && sudo yum upgrade -y 
		checkCommand
	else
		printf "\n%bdistribution not yet supported...%b\n" "${yellow}" "${rescolor}"
		exit 1
	fi
}

#installation of dependancies for Python
installDepPython () {
	noError="false"
	if [ "$OS" = "Debian GNU/Linux" ] || [ "$OS" = "Ubuntu" ] || [ "$OS" = "Raspbian GNU/Linux" ]; then
		sudo apt-get install build-essential sqlite3 bzip2 libbz2-dev zlib1g-dev openssl libgdbm-dev liblzma-dev libreadline-dev libffi-dev apt-transport-https libssl-dev curl -y # libssl-dev is required otherwise build scrypt failed (error @Shinra)
		checkCommand
	elif [ "$OS" = "CentOS Linux" ]; then
		sudo yum install build-essential sqlite3 bzip2 openssl openssl-devel bzip2-devel libffi-devel libssl-devel curl -y # libssl-devel is required otherwise build scrypt failed (error @Shinra)
		checkCommand
	else
		printf "\n%bdistribution not yet supported...%b\n" "${yellow}" "${rescolor}"
		exit 1
	fi
}

installDepPythonForGMixer () {
	noError="false"
	if [ "$OS" = "Debian GNU/Linux" ] || [ "$OS" = "Ubuntu" ] || [ "$OS" = "Raspbian GNU/Linux" ]; then
		sudo apt-get install python3-pip python3-setuptools python3-wheel python3 python-dev python3-dev build-essential libffi-dev libxml2-dev libxslt1-dev zlib1g-dev -y 2> /dev/null
		sudo pip3 install --upgrade pip 2> /dev/null
		sudo pip3 install libnacl duniterpy silkaj py-ubjson plyvel PySocks 2> /dev/null
		checkCommand
		# seulement si antérieure à 3.6, entrer la ligne suivante
		#sudo pip3 install python2-secrets
	elif [ "$OS" = "CentOS Linux" ]; then
		#todo
		echo "todo"
	else
		printf "\n%bdistribution not yet supported...%b\n" "${yellow}" "${rescolor}"
		exit 1
	fi
}

#installation of dependancies for Gmixer
installDepGMixer () {
	noError="false"
	if [ "$OS" = "Debian GNU/Linux" ] || [ "$OS" = "Ubuntu" ] || [ "$OS" = "Raspbian GNU/Linux" ]; then
		sudo apt-get install libsodium-dev -y
		checkCommand
	elif [ "$OS" = "CentOS Linux" ]; then
		sudo yum install libsodium-devel -y
		checkCommand
	else
		printf "\n%bdistribution not yet supported...%b\n" "${yellow}" "${rescolor}"
		exit 1
	fi
}


installPython () {

	if [ "$OS" = "Debian GNU/Linux" ] || [ "$OS" = "Ubuntu" ] || [ "$OS" = "Raspbian GNU/Linux" ]; then

		if [ ! -d "/usr/src/Python-3.7.3" ]; then 
			cd /usr/src
			sudo rm -rf Python-3.7.3.tgz
			sudo wget -q https://www.python.org/ftp/python/3.7.3/Python-3.7.3.tgz
			sudo tar xzf Python-3.7.3.tgz
			sudo rm -f /usr/src/Python-3.7.3.tgz
			cd Python-3.7.3
			sudo ./configure --enable-optimizations
			sudo make altinstall
		else
			printf "\n%bPython 3.7.3 is already installed, nothing to do...%b\n" "${green}" "${rescolor}"
		fi


	elif [ "$OS" = "CentOS Linux" ]; then
		
		if [ ! -d "/usr/src/Python-3.7.3" ]; then 
			cd /usr/src
			sudo wget -q https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tgz
			sudo tar xzf Python-3.7.0.tgz
			sudo rm -f /usr/src/Python-3.7.0.tgz
			cd Python-3.7.0
			sudo ./configure --enable-optimizations
			sudo make altinstall

			#centos to modify
			#sudo yum install python36 -y
			#sudo yum install python36-devel -y
			#sudo yum install python36-setuptools -y
			#sudo easy_install-3.6 pip -y
			#sudo yum remove python-setuptools
			#sudo yum install python-setuptools  
			#sudo yum install python36-pip
			#printf "\n%bInstallation of package manager for python : pip3...%b\n" "${yellow}" "${rescolor}"
			#sudo yum install epel-release
			#sudo yum install python36-setuptools
			#sudo easy_install-3.6 pip3
			#sudo python -m pip install --upgrade pip
			#sudo yum install python-pkg-resources
			#sudo pip3 install setuptools
			#sudo pip3 install libnacl duniterpy silkaj py-ubjson plyvel PySocks
			# seulement si antérieure à 3.6, entrer la ligne suivante
			#sudo pip3 install python2-secrets

		else
			printf "\n%bPython 3.7.3 is already installed, nothing to do...%b\n" "${green}" "${rescolor}"
		fi 
	else
		printf "\n%bdistribution not yet supported...%b\n" "${yellow}" "${rescolor}"
		exit 1
	fi




}


checkInstallCmake () {
	
	noError="false"
	if [ "$OS" = "Debian GNU/Linux" ] || [ "$OS" = "Ubuntu" ] || [ "$OS" = "Raspbian GNU/Linux" ]; then
		vCmake=$(cmake --version)
		cmakeStr="cmake version 3.14.3"
		if [ -z "${vCmake##*$cmakeStr*}" ]
		then 
			printf "\n%bCmake is well installed...%b\n" "${green}" "${rescolor}"
			noError="true"
		else 
			printf "\n%bCmake is not correctly installed, please check your cmake installation...%b\n" "${red}" "${rescolor}"
			noError="false"
			exit 1
		fi
	elif [ "$OS" = "CentOS Linux" ]; then
		vCmake=$(cmake --version)
		cmakeStr="cmake version 3.14.3"
		if [ -z "${vCmake##*$cmakeStr*}" ]
		then 
			printf "\n%bCmake is well installed...%b\n" "${green}" "${rescolor}"
			noError="true"
		else 
			printf "\n%bCmake is not correctly installed, please check your cmake installation...%b\n" "${red}" "${rescolor}"
			noError="false"
			exit 1
		fi
	else
		printf "\n%bdistribution not yet supported...%b\n" "${yellow}" "${rescolor}"
		exit 1
	fi

}




checkInstallPython () {
	
	noError="false"
	if [ "$OS" = "Debian GNU/Linux" ] || [ "$OS" = "Ubuntu" ] || [ "$OS" = "Raspbian GNU/Linux" ]; then
		vPython=$(python3.7 -V)
		pythonStr="Python"
		if [ -z "${vPython##*$pythonStr*}" ]
		then 
			printf "\n%bPython is well installed...%b\n" "${green}" "${rescolor}"
			noError="true"
		else 
			printf "\n%bPython is not correctly installed, please check your python installation...%b\n" "${red}" "${rescolor}"
			noError="false"
			exit 1
		fi
	elif [ "$OS" = "CentOS Linux" ]; then
		vPython=$(python3.6 -V)
		pythonStr="Python"
		if [ -z "${vPython##*$pythonStr*}" ]
		then 
			printf "\n%bPython is well installed...%b\n" "${green}" "${rescolor}"
			noError="true"
		else 
			printf "\n%bPython is not correctly installed, please check your python installation...%b\n" "${red}" "${rescolor}"
			noError="false"
			exit 1
		fi
	else
		printf "\n%bdistribution not yet supported...%b\n" "${yellow}" "${rescolor}"
		exit 1
	fi

}


launchGMixer () {
	noError="false"
	if [ "$OS" = "Debian GNU/Linux" ] || [ "$OS" = "Ubuntu" ] || [ "$OS" = "Raspbian GNU/Linux" ]; then
		$(which python3.7) $HOME/gmixer-py/server.py -s -d $HOME/.gmixer
		checkCommand
		if test $noError = "false"
		then
			$(which python3) $HOME/gmixer-py/server.py -s -d $HOME/.gmixer
		fi

	elif [ "$OS" = "CentOS Linux" ]; then
		$(which python3.6) $HOME/gmixer-py/server.py -s -d $HOME/.gmixer
	else
		printf "\n%bdistribution not yet supported...%b\n" "${yellow}" "${rescolor}"
		exit 1
	fi

}

installOrBuildCmake () {

	if [ "$OS" = "Debian GNU/Linux" ] || [ "$OS" = "Ubuntu" ] || [ "$OS" = "Raspbian GNU/Linux" ]; then

		if [ ! -d "/usr/src/cmake-3.14.3" ]; then 
			cd /usr/src
			sudo rm -f /usr/src/cmake.tar.gz
			#sudo apt install build-essential cmake -y #pour installer cmake nécessaire au build de leveldb >=1.21 #fonctionne sur debian stretch 9.8
			#si installation au-dessus échoue, construire à la main cmake
			sudo wget -q https://github.com/Kitware/CMake/releases/download/v3.14.3/cmake-3.14.3.tar.gz -O cmake.tar.gz
			sudo tar -xvf cmake.tar.gz
			cd cmake-3.14.3/
			sudo chmod +x bootstrap
			sudo ./bootstrap
			sudo make && sudo make install
		else
			printf "\n%bcmake 3.14.3 is already installed, nothing to do...%b\n" "${green}" "${rescolor}"
		fi
	elif [ "$OS" = "CentOS Linux" ]; then

		if [ ! -d "/usr/src/cmake-3.14.3" ]; then 
			cd /usr/src
			sudo rm -f /usr/src/cmake.tar.gz
			#sudo yum install build-essential cmake -y #pour installer cmake nécessaire au build de leveldb >=1.21 #fonctionne sur debian stretch 9.8
			#si installation au-dessus échoue, construire à la main cmake
			sudo wget -q https://github.com/Kitware/CMake/releases/download/v3.14.3/cmake-3.14.3.tar.gz -O cmake.tar.gz
			sudo tar -xvf cmake.tar.gz
			cd cmake-3.14.3/
			sudo chmod +x bootstrap
			sudo ./bootstrap
			sudo make && sudo make install
		else
			printf "\n%bcmake 3.14.3 is already installed, nothing to do...%b\n" "${green}" "${rescolor}"
		fi
	else
		printf "\n%bdistribution not yet supported...%b\n" "${yellow}" "${rescolor}"
		exit 1
	fi

}

checkBuildLevelDB () {

	if [ -f "/usr/local/lib/libleveldb.a" ]; then
		noError="true"
	else
		noError="false"
	fi

}

buildLevelDB () {
	noError="false"
	if [ "$OS" = "Debian GNU/Linux" ] || [ "$OS" = "Ubuntu" ] || [ "$OS" = "Raspbian GNU/Linux" ]; then
		cd /usr/src
		sudo rm -rf /usr/src/1.21.tar.gz
		#for plylevel install leveldb >=1.21 (build it by hand on debian stretch, CMake 3.9 or higher is required)
		sudo wget -q https://github.com/google/leveldb/archive/1.21.tar.gz
		sudo tar xvzf 1.21.tar.gz
		cd leveldb-1.21
		sudo mkdir -p build && cd build
		sudo cmake -DCMAKE_BUILD_TYPE=Release .. && sudo cmake --build .
		sudo mv libleveldb.* /usr/local/lib
		sudo ldconfig

	elif [ "$OS" = "CentOS Linux" ]; then
		cd /usr/src
		sudo rm -rf /usr/src/1.21.tar.gz
		#for plylevel install leveldb >=1.21 (build it by hand on debian stretch, CMake 3.9 or higher is required)
		sudo wget -q https://github.com/google/leveldb/archive/1.21.tar.gz
		sudo tar xvzf 1.21.tar.gz
		cd leveldb-1.21
		sudo mkdir -p build && cd build
		sudo cmake -DCMAKE_BUILD_TYPE=Release .. && sudo cmake --build .
		sudo mv libleveldb.* /usr/local/lib
		sudo ldconfig
	else
		printf "\n%bdistribution not yet supported...%b\n" "${yellow}" "${rescolor}"
		exit 1
	fi





}

checkTorInstallation () {
	noError="false"
	vTor=$(tor --version)
	torStr="Tor version"
	if [ -z "${vTor##*$torStr*}" ]
	then 
		printf "\n%bTor is well installed...%b\n" "${green}" "${rescolor}"
			noError="true"
	else 
		printf "\n%Tor is not correctly installed, please check your Tor installation...%b\n" "${red}" "${rescolor}"
		noError="false"
		exit 1
	fi
}

openPortGMixer () {

	noError="false"
	if [ "$OS" = "Debian GNU/Linux" ] || [ "$OS" = "Ubuntu" ] || [ "$OS" = "Raspbian GNU/Linux" ]; then

		sudo iptables -A INPUT -p tcp -m tcp --dport "$@" -j ACCEPT
		sudo iptables -A OUTPUT -p tcp -m tcp --dport "$@" -j ACCEPT
		checkCommand

	elif [ "$OS" = "CentOS Linux" ]; then
		sudo firewall-cmd --permanent --add-port="$@"/tcp
		sudo firewall-cmd --reload
		checkCommand
	else
		printf "\n%bdistribution not yet supported...%b\n" "${yellow}" "${rescolor}"
		exit 1
	fi



}


modifiyConfigurationFile () {

	noError="false"
	if [ "$OS" = "Debian GNU/Linux" ] || [ "$OS" = "Ubuntu" ] || [ "$OS" = "Raspbian GNU/Linux" ]; then
		printf "%bretrieve public ip...%b" "${yellow}" "${rescolor}"
		ip=$( getPublicIp )
		sed -i "s/127.0.1.1/`echo $ip`/g" $HOME/.gmixer/config.json
		if test "$parameterT" = "2" || test "$parameterT" = "3"
		then
			sudo sed -i "s/"null,"/"[\"127.0.0.1\",9050],"/g" $HOME/.gmixer/config.json
			sudo sed -i "s/"false"/"true"/g" $HOME/.gmixer/config.json
		fi
		checkCommand
	elif [ "$OS" = "CentOS Linux" ]; then
		printf "ok"
	else
		printf "\n%bdistribution not yet supported...%b\n" "${yellow}" "${rescolor}"
		exit 1
	fi



}

installationTor () {
	noError="false"
	curl -s https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --import 2>/dev/null
	gpg --export A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89 | sudo apt-key add - 2>/dev/null
	numberOccTorProject=$(grep -c "deb.torproject.org" /etc/apt/sources.list)
	if test "$numberOccTorProject" = "0"
	then
		echo "deb http://deb.torproject.org/torproject.org stretch main" | sudo tee -a /etc/apt/sources.list
		echo "deb-src http://deb.torproject.org/torproject.org stretch main" | sudo tee -a /etc/apt/sources.list
	fi
	sudo apt-get update -y && sudo apt-get upgrade -y
	sudo apt-get install tor deb.torproject.org-keyring -y

}

configurationTor () {
	noError="false"
	numberOccHiddenServiceDir=$(grep -c "#HiddenServiceDir" /etc/tor/torrc)
	numberOccHiddenServicePort=$(grep -c "#HiddenServicePort" /etc/tor/torrc)
	if test "$numberOccHiddenServiceDir" = "2" && test "$numberOccHiddenServicePort" = "3"
	then
		printf "ok"
		sudo sed -i "s/"#DataDirectory"/"DataDirectory"/g" /etc/tor/torrc
		sudo sed -ie '0,/#HiddenServiceDir/ s/#HiddenServiceDir/HiddenServiceDir/' /etc/tor/torrc 
		sudo sed -ie '0,/#HiddenServicePort/ s/#HiddenServicePort/HiddenServicePort/' /etc/tor/torrc 
		checkCommand
	fi



	if test "$numberOccHiddenServiceDir" = "1" && test "$numberOccHiddenServicePort" = "2"
	then
		noError="true"
	fi
}

generateRandomSalt () {
	salt=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-44} | head -n 1)
	echo $salt

}

generateRandomPass () {
	pass=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-44} | head -n 1)
	echo $pass
}

main () {

	printf "\n%bUpdate system...%b" "${yellow}" "${rescolor}"
	updateSystem > /dev/null
	printOkKo
	printf "%bInstallation of dependancies for installing python 3.7...%b" "${yellow}" "${rescolor}"
	installDepPython > /dev/null
	printOkKo
	printf "%bBuild of python 3.7, this step may take time, don't hesitate to take a coffee...%b" "${yellow}" "${rescolor}"
	installPython > /dev/null
	checkInstallPython > /dev/null
	printOkKo
	printf "%bInstallation of dependancies for gmixer-py...%b" "${yellow}" "${rescolor}"
	installDepGMixer > /dev/null 
	printOkKo
	#for centos 
	#printf "\n%bInstallation of g++...%b\n" "${yellow}" "${rescolor}"
	#sudo yum -y install gcc-c++
	printf "%bBuild of cmake 3.14.3 (necessary for leveldb 1.21), this step may take time, don't hesitate to take a tea...%b" "${yellow}" "${rescolor}"
	installOrBuildCmake > /dev/null
	checkInstallCmake > /dev/null
	printOkKo
	printf "%bBuild of leveldb 1.21, this step may take time, don't hesitate to take a beer....%b" "${yellow}" "${rescolor}"
	buildLevelDB > /dev/null
	checkBuildLevelDB > /dev/null
	printOkKo
	printf "%bInstallation of python dependancies for gmixer...%b" "${yellow}" "${rescolor}"
	installDepPythonForGMixer > /dev/null 
	printOkKo
	printf "%bRetrieve project gmixer from gitlab...%b" "${yellow}" "${rescolor}"
	getGmixerProject > /dev/null
	printOkKo
	printf "%bGenerate configuration folder : $HOME/.gmixer...%b" "${yellow}" "${rescolor}"
	generateConfFolder > /dev/null
	printOkKo
	if [ ! -z "$parameterP" ] && [ "$parameterP" != "10951" ]
	then
		portUse1=$(sudo lsof -i -P -n | grep LISTEN | grep $parameterP | wc -l)
		portUse2=$(sudo netstat -tulpn | grep LISTEN | grep $parameterP | wc -l)
		if test ${portUse1} = "0" && test ${portUse2} = "0"
		then
			sed -i "s/\"bind_port\": 10951,/\"bind_port\": `echo $parameterP`,/g" $HOME/.gmixer/config.json
			sed -i "s/\"public_port\": 10951/\"public_port\": `echo $parameterP`/g" $HOME/.gmixer/config.json
			printf "%bOpen port %s...%b" "${yellow}" "${parameterP}" "${rescolor}"
			openPortGMixer ${parameterP}
			printOkKo
		else
			printf "%bPort %s already used, default port 10951 will be used...%b" "${yellow}" "${parameterP}" "${rescolor}"
			printf "%bOpen port 10951 (default port)...%b" "${yellow}" "${rescolor}"
			openPortGMixer 10951
			printOkKo
		fi
	else
		printf "%bOpen port 10951 (default port)...%b" "${yellow}" "${rescolor}"
		openPortGMixer 10951
		printOkKo
	fi
	printf "%bModify configuration file...%b" "${yellow}" "${rescolor}"
	modifiyConfigurationFile > /dev/null
	printOkKo
	if test "$parameterT" = "2" || test "$parameterT" = "3"
	then
	  printf "%bInstallation of Tor...%b" "${yellow}" "${rescolor}"
	  installationTor > /dev/null
	  checkTorInstallation > /dev/null
	  printOkKo
	  if test "$parameterT" = "3"
	  then
     	    printf "%bConfiguration of tor...%b" "${yellow}" "${rescolor}"
	    configurationTor > /dev/null
	    printOkKo
	    printf "%bReload configuration...%b" "${yellow}" "${rescolor}"
	    sudo systemctl reload tor > /dev/null
	    noError="false"
	    checkCommand
	    printOkKo
          fi
	  printf "%bRestart of tor...%b" "${yellow}" "${rescolor}"
	  sudo systemctl restart tor > /dev/null
	  noError="false"
	  checkCommand
	  printOkKo
	  if test "$parameterT" = "3"
	  then
	    printf "%bYour onion domain is :%b\n" "${yellow}" "${rescolor}"
	    if [ ! -f "/var/lib/tor/hidden_service/hostname" ]
	    then
		sudo cat "/var/lib/tor/hidden_service/hostname"
		printf "%bCopy this onion domain in the gmixer config.json in public_host and bind_host :%b\n" "${yellow}" "${rescolor}"
	    else
		printf "%b/var/lib/tor/hidden_service/hostname doesn't exists, check your configuration of tor%b\n" "${red}" "${rescolor}"
		
	    fi
          fi
	fi
	if [ "$parameterG" = "true" ]
	then
		printf "%bGenerate Salt and Password...%b\n" "${yellow}" "${rescolor}"
		salt=$(generateRandomSalt)
		password=$(generateRandomPass)
		sed -i "s/\"id_salt\": \"\",/\"id_salt\": \"`echo $salt`\",/g" $HOME/.gmixer/config.json
		sed -i "s/\"id_password\": \"\"/\"id_password\": \"`echo $password`\"/g" $HOME/.gmixer/config.json
		
	else
		printf "%bEnter Salt and Password...%b\n" "${yellow}" "${rescolor}"
		printf "%bEnter Salt:%b" "${blue}" "${rescolor}"
		readSecret salt
		printf "%bEnter Password:%b" "${blue}" "${rescolor}"
		readSecret password
		sed -i "s/\"id_salt\": \"\",/\"id_salt\": \"`echo $salt`\",/g" $HOME/.gmixer/config.json
		sed -i "s/\"id_password\": \"\"/\"id_password\": \"`echo $password`\"/g" $HOME/.gmixer/config.json
	fi
	printf "%bLaunch GMixer : %b\n" "${yellow}" "${rescolor}"
	launchGMixer

}



mainVerbose () {

	printf "\n%bUpdate system...%b" "${yellow}" "${rescolor}"
	updateSystem && sleep 1 
	printOkKo
	printf "%bInstallation of dependancies for installing python 3.7...%b" "${yellow}" "${rescolor}"
	installDepPython && sleep 1
	printOkKo
	printf "%bBuild of python 3.7, this step may take time, don't hesitate to take a coffee...%b" "${yellow}" "${rescolor}"
	installPython 
	checkInstallPython && sleep 1
	printOkKo
	printf "%bInstallation of dependancies for gmixer-py...%b" "${yellow}" "${rescolor}"
	installDepGMixer && sleep 1
	printOkKo
	#for centos 
	#printf "\n%bInstallation of g++...%b\n" "${yellow}" "${rescolor}"
	#sudo yum -y install gcc-c++
	printf "%bBuild of cmake 3.14.3 (necessary for leveldb 1.21), this step may take time, don't hesitate to take a tea...%b" "${yellow}" "${rescolor}"
	installOrBuildCmake && sleep 1
	checkInstallCmake && sleep 1
	printOkKo
	printf "%bBuild of leveldb 1.21, this step may take time, don't hesitate to take a beer....%b" "${yellow}" "${rescolor}"
	buildLevelDB && sleep 1
	checkBuildLevelDB && sleep 1
	printOkKo
	printf "%bInstallation of python dependancies for gmixer...%b" "${yellow}" "${rescolor}"
	installDepPythonForGMixer && sleep 1 
	printOkKo
	printf "%bRetrieve project gmixer from gitlab...%b" "${yellow}" "${rescolor}"
	getGmixerProject && sleep 1
	printOkKo
	printf "%bGenerate configuration folder : $HOME/.gmixer...%b" "${yellow}" "${rescolor}"
	generateConfFolder && sleep 1 
	printOkKo
	if [ ! -z "$parameterP" ] && [ "$parameterP" != "10951" ]
	then
		portUse1=$(sudo lsof -i -P -n | grep LISTEN | grep $parameterP | wc -l)
		portUse2=$(sudo netstat -tulpn | grep LISTEN | grep $parameterP | wc -l)
		if test ${portUse1} = "0" && test ${portUse2} = "0"
		then
			sed -i "s/\"bind_port\": 10951,/\"bind_port\": `echo $parameterP`,/g" $HOME/.gmixer/config.json
			sed -i "s/\"public_port\": 10951/\"public_port\": `echo $parameterP`/g" $HOME/.gmixer/config.json
			printf "%bOpen port %s...%b" "${yellow}" "${parameterP}" "${rescolor}"
			openPortGMixer ${parameterP}
			printOkKo
		else
			printf "%bPort %s already used, default port 10951 will be used...%b" "${yellow}" "${parameterP}" "${rescolor}"
			printf "%bOpen port 10951 (default port)...%b" "${yellow}" "${rescolor}"
			openPortGMixer 10951
			printOkKo
		fi
	else
		printf "%bOpen port 10951 (default port)...%b" "${yellow}" "${rescolor}"
		openPortGMixer 10951
		printOkKo
	fi
	printf "%bModify configuration file...%b" "${yellow}" "${rescolor}"
	modifiyConfigurationFile && sleep 1 
	printOkKo
	if test "$parameterT" = "2" || test "$parameterT" = "3"
	then
	  printf "%bInstallation of Tor...%b" "${yellow}" "${rescolor}"
	  installationTor && sleep 1
	  checkTorInstallation && sleep 1
	  printOkKo
	  if test "$parameterT" = "3"
	  then
     	    printf "%bConfiguration of tor...%b" "${yellow}" "${rescolor}"
	    configurationTor && sleep 1
	    printOkKo
	    printf "%bReload configuration...%b" "${yellow}" "${rescolor}"
	    sudo systemctl reload tor && sleep 1
	    noError="false"
	    checkCommand && sleep 1
	    printOkKo
          fi
	  printf "%bRestart of tor...%b" "${yellow}" "${rescolor}"
	  sudo systemctl restart tor && sleep 1
	  noError="false"
	  checkCommand && sleep 1
	  printOkKo
	  if test "$parameterT" = "3"
	  then
	    printf "%bYour onion domain is :%b\n" "${yellow}" "${rescolor}"
	    if [ ! -f "/var/lib/tor/hidden_service/hostname" ]
	    then
		sudo cat "/var/lib/tor/hidden_service/hostname"
		printf "%bCopy this onion domain in the gmixer config.json in public_host and bind_host :%b\n" "${yellow}" "${rescolor}"
	    else
		printf "%b/var/lib/tor/hidden_service/hostname doesn't exists, check your configuration of tor%b\n" "${red}" "${rescolor}"
	    fi
          fi
	fi
	if [ "$parameterG" = "true" ]
	then
		printf "%bGenerate Salt and Password...%b\n" "${yellow}" "${rescolor}"
		salt=$(generateRandomSalt)
		password=$(generateRandomPass)
		sed -i "s/\"id_salt\": \"\",/\"id_salt\": \"`echo $salt`\",/g" $HOME/.gmixer/config.json
		sed -i "s/\"id_password\": \"\"/\"id_password\": \"`echo $password`\"/g" $HOME/.gmixer/config.json
		checkCommand && sleep 1
	  	printOkKo
		
	else
		printf "%bEnter Salt and Password...%b\n" "${yellow}" "${rescolor}"
		printf "%bEnter Salt:%b" "${blue}" "${rescolor}"
		readSecret salt
		printf "%bEnter Password:%b" "${blue}" "${rescolor}"
		readSecret password
		sed -i "s/\"id_salt\": \"\",/\"id_salt\": \"`echo $salt`\",/g" $HOME/.gmixer/config.json
		sed -i "s/\"id_password\": \"\"/\"id_password\": \"`echo $password`\"/g" $HOME/.gmixer/config.json
	fi

	printf "%bLaunch GMixer : %b\n" "${yellow}" "${rescolor}"
	launchGMixer

}

#-----------------------------------------------------------------------------------end functions-------------------------------------------------------------



#-------------------------programm--------------------------------------------------------------------------------------------------------


parameterV="false"
parameterH="false"
parameterG="false"
parameterD="false"

while getopts "t:vhgdp:" opt
do
   case "$opt" in
      t ) parameterT="$OPTARG" ;;
      v ) parameterV="true" ;;
      h ) parameterH="true" ;;
      g ) parameterG="true" ;;
      p ) parameterP="$OPTARG" ;;
      d ) parameterD="true" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done



if [ "$parameterH" = "true" ]
then
	helpFunction
fi

if [ "$parameterV" = "false" ]; then

	printf "\n"
	helpFunctionWithoutExit
	printf "\n"
	main
	exit 0

else
	
	printf "\n"
	helpFunctionWithoutExit
	printf "\n"
	mainVerbose
	exit 0

fi



#---------------------------end of programm-----------------------------------------------------------------






