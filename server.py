#!/usr/bin/env python3

"""
	CopyLeft 2019 Pascal Engélibert <tuxmain@zettascript.org>
	This file is part of ĞMixer-py.

	ĞMixer-py is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ĞMixer-py is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with ĞMixer-py.  If not, see <https://www.gnu.org/licenses/>.
"""

import sys, os, json, asyncio, getpass, random, time, socket, secrets, base64
from threading import Thread
import ubjson
import plyvel
import libnacl.sign
import socks
import duniterpy.api.bma as bma
from duniterpy.api.client import Client
from duniterpy.key import SigningKey, PublicKey
import utils
import nest_asyncio

"""
Used terms:
sender_ = node which has sent something to me
receiver_ = node I'm sending something to
in_  = something that a node sent to me
out_ = something I'm sending to a node
"""

DIR = "~/.config/gmixer"
BIND_HOST = socket.gethostname()
try:
	BIND_HOST = socket.gethostbyname(BIND_HOST)
except socket.gaierror:
	pass
BIND_PORT = 10951
PUBLIC_HOST = BIND_HOST
PUBLIC_PORT = BIND_PORT
ID_SALT = ""
ID_PASSWORD = ""
BMA_HOSTS = ["BASIC_MERKLED_API 149.91.88.175 10901", "BMAS g1.duniter.fr 443", "BMAS g1.duniter.org 443", "BMAS g1.presles.fr 443", "BMAS g1.cgeek.fr 443", "BMAS ts.g1.librelois.fr 443"]
MIX_INTERVAL = 120
MIX_MIN_TXS = 5 # minimum amount of txs to mix
MIX_REQ_AGE_MAX = 604800 # maximum mix request age before return to sender
PEER_INFO_INTERVAL = 600 # interval for renewing my peer document
PEER_SIG_AGE_MAX = 604800 # max age of a peer document signature
PEER_DETECT_INTERVAL = 120 # interval for fetching peer list
IDTY_SIG_AGE_MAX = 2592000 # max age of a idty document signature
CURRENCY = "g1"

def send_response(client, code, resp, dataformat="ubjson"):
	if dataformat == "ubjson":
		content_raw = ubjson.dumpb(resp)
		mime = "application/ubjson"
	elif dataformat == "json":
		try:
			content_raw = json.dumps(resp).encode()
		except TypeError:
			content_raw = json.dumps({"error": "non_ascii_resp"}).encode()
		mime = "text/json"
	client.sendall(("HTTP/1.1 "+code+"\r\nContent-type: "+mime+"; charset=UTF-8\r\nAccess-Control-Allow-Origin: *\r\nContent-length: "+str(len(content_raw))+"\r\n\r\n").encode()+content_raw)
	client.close()

class TX:
	def __init__(self, sender_pubkey=None, receiver_pubkey=None, onetime_pubkey=None, in_amount=None, in_base=None, out_amount=None, out_base=None, message=None, in_seeds=None, out_seeds=None, send_confirm=True, date=None, expire=None):
		self.sender_pubkey = sender_pubkey
		self.receiver_pubkey = receiver_pubkey
		self.onetime_pubkey = onetime_pubkey
		self.in_amount = in_amount
		self.in_base = in_base
		self.out_amount = out_amount
		self.out_base = out_base
		self.message = message
		self.in_seeds = in_seeds
		self.out_seeds = out_seeds
		self.send_confirm = send_confirm # True if sender is a server (in which case we send confirmation to it); False else (we wait a confirmation demand from it)
		self.date = date
		self.expire = expire
		
		self.need_send = True
		self.can_confirm = False
		self.need_confirm = True
		self.confirms = b""
		self.sender_hash = None
		self.receiver_hash = None
		self.tx_sent = False
	
	def gen_mix_confirm(self, keys):
		message = {
			"document": "gmixer-mixconfirm1",
			"sender_pubkey": self.sender_pubkey,
			"receiver_pubkey": keys.pubkey,
			"in_amount": self.in_amount,
			"in_base": self.in_base,
			"in_seeds": self.in_seeds,
			"receiver_pubkey": self.receiver_pubkey,
			"out_amount": self.out_amount,
			"out_base": self.out_base,
			"out_seeds": self.out_seeds,
			"req_date": self.date,
			"expire_date": self.expire
		}
		message = ubjson.dumpb(message)
		message = keys.sign(message)
		message = PublicKey(self.onetime_pubkey).encrypt_seal(message)
		message = self.in_seeds[1] + self.in_seeds[2] + utils.int_to_bin(len(message)) + message + self.confirms
		message = PublicKey(self.sender_pubkey).encrypt_seal(message)
		message = keys.sign(message)
		return message
	
	def export_ubjson(self, db_txs):
		db_txs.put(self.in_seeds[2], ubjson.dumpb({
			"sender_pubkey": self.sender_pubkey,
			"receiver_pubkey": self.receiver_pubkey,
			"onetime_pubkey": self.onetime_pubkey,
			"in_amount": self.in_amount,
			"in_base": self.in_base,
			"out_amount": self.out_amount,
			"out_base": self.out_base,
			"message": self.message,
			"in_seeds": self.in_seeds,
			"out_seeds": self.out_seeds,
			"send_confirm": self.send_confirm,
			"date": self.date,
			"expire": self.expire,
			"need_send": self.need_send,
			"can_confirm": self.can_confirm,
			"need_confirm": self.need_confirm,
			"confirms": self.confirms,
			"tx_sent": self.tx_sent,
			"sender_hash": self.sender_hash,
			"receiver_hash": self.receiver_hash
		}))
	
	def import_ubjson(d):
		d = ubjson.loadb(d)
		tx = TX(d["sender_pubkey"], d["receiver_pubkey"], d["onetime_pubkey"], d["in_amount"], d["in_base"], d["out_amount"], d["out_base"], d["message"], d["in_seeds"], d["out_seeds"], d["send_confirm"], d["date"], d["expire"])
		tx.need_send = d["need_send"]
		tx.can_confirm = d["can_confirm"]
		tx.need_confirm = d["need_confirm"]
		tx.confirms = d["confirms"]
		tx.tx_sent = d["tx_sent"]
		tx.sender_hash = d["sender_hash"]
		tx.receiver_hash = d["receiver_hash"]
		return tx

def load_txs(db_txs, pool, tx_in_index, tx_out_index):
	for _, data in db_txs:
		tx = TX.import_ubjson(data)
		pool.append(tx)
		tx_in_index[tx.in_seeds[1]] = tx
		tx_out_index[tx.out_seeds[1]] = tx
	
	utils.logprint("Loaded "+str(len(pool))+" txs", utils.LOG_TRACE)

def save_txs(db_txs, pool):
	for tx in pool:
		tx.export_ubjson(db_txs)

# Read json config file
def read_config(cdir, conf_overwrite={}):
	if not os.path.isfile(cdir+"/config.json"):
		configfile = open(cdir+"/config.json", "w")
		configfile.write("{}")
		configfile.close()
	
	with open(cdir+"/config.json", "r") as configfile:
		try:
			conf = json.load(configfile)
		except json.JSONDecodeError:
			utils.logprint("Config: bad JSON => abort", utils.LOG_ERROR)
			exit(1)
	
	conf.setdefault("currency", CURRENCY)
	conf.setdefault("server", {})
	conf["server"].setdefault("bind_host", BIND_HOST)
	conf["server"].setdefault("bind_port", BIND_PORT)
	conf["server"].setdefault("public_host", PUBLIC_HOST)
	conf["server"].setdefault("public_port", PUBLIC_PORT)
	conf["server"].setdefault("peer_info_interval", PEER_INFO_INTERVAL)
	conf["server"].setdefault("peer_sig_age_max", PEER_SIG_AGE_MAX)
	conf["server"].setdefault("peer_detect_interval", PEER_DETECT_INTERVAL)
	conf["server"].setdefault("idty_sig_age_max", IDTY_SIG_AGE_MAX)
	conf.setdefault("client", {})
	conf["client"].setdefault("bma_hosts", BMA_HOSTS)
	conf["client"].setdefault("proxy", None)
	conf["client"].setdefault("proxy_onion_only", False)
	conf.setdefault("crypto", {})
	conf["crypto"].setdefault("id_salt", ID_SALT)
	conf["crypto"].setdefault("id_password", ID_PASSWORD)
	conf.setdefault("mix", {})
	conf["mix"].setdefault("mix_interval", MIX_INTERVAL)
	conf["mix"].setdefault("mix_min_txs", MIX_MIN_TXS)
	conf["mix"].setdefault("mix_req_age_max", MIX_REQ_AGE_MAX)
	conf.setdefault("idty", {})
	conf["idty"].setdefault("needed", True)
	conf["idty"].setdefault("sig", "")
	conf["idty"].setdefault("pubkey", "")
	
	for key in conf_overwrite:
		c = conf
		k = key.split(".")
		for i in k[:len(k)-1]:
			c = conf[i]
		c[k[len(k)-1]] = conf_overwrite[key]
	
	with open(cdir+"/config.json", "w") as configfile:
		json.dump(conf, configfile, indent=1)
	
	return conf

class ServerThread(Thread):
	def __init__(self, conf, peers, keys, local_peer, pool, tx_in_index, tx_out_index, db_txs, timers, client_th):
		Thread.__init__(self)
		
		self.conf = conf
		self.peers = peers
		self.keys = keys
		self.local_peer = local_peer
		self.pool = pool
		self.tx_in_index = tx_in_index
		self.tx_out_index = tx_out_index
		self.db_txs = db_txs
		self.timers = timers
		self.client_th = client_th
		
		self.sock = None
		self.work = True
		
	def run(self):
		server_addr = (self.conf["server"]["bind_host"], self.conf["server"]["bind_port"])
		if ":" in server_addr[0]: # IPv6
			self.sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
		else: # IPv4
			self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.sock.settimeout(5)
		self.sock.bind(server_addr)
		self.sock.listen(1)
		utils.logprint("Server started at "+str(server_addr), utils.LOG_INFO)
		
		while self.work:
			try:
				client, addr = self.sock.accept()
			except socket.timeout:
				continue
			
			# Get request
			paquet = b""
			header = b""
			content = b""
			content_len = 0
			resp = {}
			lf = 0
			while True:
				raw = client.recv(utils.RECBUF)
				if raw:
					paquet += raw
					if lf >= 0:
						for c in raw:
							if c == 10:# LF
								lf += 1
							elif c != 13:# CR
								lf = 0
							if lf > 1:
								parts = paquet.split(b"\r\n\r\n")
								header = parts[0]
								content = parts[1]
								try:
									content_len = int(utils.p_clen.search(header.decode()).group(1))
								except (AttributeError, ValueError):
									content_len = 0
								break
						if lf > 1:
							break
				else:
					break
			while len(content) < content_len:
				raw = client.recv(utils.RECBUF)
				paquet += raw
				content += raw
			
			# Get URL
			httpreq = paquet.split(b"\n")
			try:
				url = httpreq[0].split(b" ")[1].decode().split("/")
			except IndexError:
				send_response(client, "400 Bad Request", {"error": "bad_http"})
				continue
			while "" in url:
				url.remove("")
			urll = len(url)
			
			resp = {}
			resp_format = "json" if "json" in url else "ubjson"
			
			# Treat request
			if "pubkey" in url:
				resp["pubkey"] = self.keys.pubkey
			
			if "version" in url:
				resp["version"] = utils.VERSION
			
			if "mix" in url:
				entry_node = "client" in url
				
				if entry_node:
					sender_pubkey = utils.getargv("mix", "", 1, url)
					
					try:
						sender_keys = PublicKey(sender_pubkey)
					except ValueError:
						send_response(client, "401 Unauthorized", {"error": "bad_sender_pubkey"}, resp_format)
						continue
				else:
					try:
						sender_hash = base64.urlsafe_b64decode(utils.getargv("mix", "", 1, url))
					except base64.binascii.Error:
						send_response(client, "401 Unauthorized", {"error": "bad_url"}, resp_format)
						continue
					
					if not sender_hash in self.peers:
						send_response(client, "401 Unauthorized", {"error": "unknown_peer"}, resp_format)
						continue
					
					peer = self.peers[sender_hash]
					sender_pubkey = peer.pubkey
					sender_keys = peer.keys
				
				try:
					in_amount = int(utils.getargv("mix", "", 2, url))
					in_base = int(utils.getargv("mix", "", 3, url))
				except ValueError:
					send_response(client, "401 Unauthorized", {"error": "bad_amount_base"}, resp_format)
					continue
				
				try:
					raw = libnacl.sign.Verifier(sender_keys.hex_pk()).verify(content) # Verify
				except ValueError:
					send_response(client, "401 Unauthorized", {"error": "bad_signature"}, resp_format)
					continue
				
				try:
					data = self.keys.decrypt_seal(raw[32:]) # Decrypt
				except libnacl.CryptError:
					send_response(client, "403 Forbidden", {"error": "bad_encryption"}, resp_format)
					continue
				
				try:
					data = ubjson.loadb(data)
					assert "receiver" in data and type(data["receiver"]) == str
					assert "onetime" in data and type(data["onetime"]) == str
					assert "in_seeds" in data and type(data["in_seeds"]) == list and len(data["in_seeds"]) == 3 and type(data["in_seeds"][0]) == bytes and len(data["in_seeds"][0]) == 32 and data["in_seeds"][1] == None and data["in_seeds"][2] == None
					assert "out_seeds" in data and type(data["out_seeds"]) == list and len(data["out_seeds"]) == 3  and type(data["out_seeds"][0]) == bytes and len(data["out_seeds"][0]) == 32 and data["out_seeds"][1] == None
					assert "message" in data and type(data["message"]) == bytes
					assert (data["message"] == b"" and type(data["out_seeds"][2]) == bytes and len(data["out_seeds"][2]) == 32) or (data["message"] != b"" and data["out_seeds"][2] == None)
				except (ubjson.decoder.DecoderException, AssertionError):
					send_response(client, "403 Forbidden", {"error": "bad_ubjson"}, resp_format)
					continue
				
				receiver_pubkey = data["receiver"] # receiver pubkey
				onetime_pubkey = data["onetime"] # origin emitter one-time pubkey
				in_seeds = data["in_seeds"]
				out_seeds = data["out_seeds"]
				message = data["message"]
				
				in_seeds[1] = raw[:32]
				in_seeds[2] = secrets.token_bytes(32)
				out_seeds[1] = secrets.token_bytes(32)
				
				try:
					PublicKey(receiver_pubkey)
				except ValueError:
					send_response(client, "403 Forbidden", {"error": "bad_rec_pubkey"}, resp_format)
					continue
				
				try:
					PublicKey(onetime_pubkey)
				except ValueError:
					send_response(client, "403 Forbidden", {"error": "bad_onetime_pubkey"}, resp_format)
					continue
				
				# Save tx in pool
				t = time.time()
				tx = TX(sender_pubkey, receiver_pubkey, onetime_pubkey, in_amount, in_base, in_amount, in_base, message, in_seeds, out_seeds, not entry_node, t, t+self.conf["mix"]["mix_req_age_max"])
				last_node = len(message) == 0
				tx.need_send = not last_node
				tx.can_confirm = last_node
				if not entry_node:
					tx.sender_hash = peer.hash
					peer.up_in = True
				utils.logprint("TX "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
				self.tx_out_index[out_seeds[1]] = tx
				self.tx_in_index[in_seeds[1]] = tx
				self.pool.append(tx)
				tx.export_ubjson(self.db_txs)
				
				resp["mix_ok"] = in_seeds[1]
			
			elif "confirm" in url:
				try:
					receiver_hash = base64.urlsafe_b64decode(utils.getargv("confirm", "", 1, url))
				except base64.binascii.Error:
					send_response(client, "401 Unauthorized", {"error": "bad_url"}, resp_format)
					continue
				
				try:
					out_seed1 = bytes.fromhex(utils.getargv("confirm", "", 2, url))
				except ValueError:
					send_response(client, "401 Unauthorized", {"error": "bad_url"}, resp_format)
					continue
				
				if not receiver_hash in self.peers:
					send_response(client, "401 Unauthorized", {"error": "unknown_peer"}, resp_format)
					continue
				
				peer = self.peers[receiver_hash]
				
				if not out_seed1 in self.tx_out_index:
					send_response(client, "404 Not Found", {"error": "unknown_tx"}, resp_format)
					continue
				
				tx = self.tx_out_index[out_seed1]
				
				if peer.hash != tx.receiver_hash:
					send_response(client, "404 Not Found", {"error": "unknown_tx"}, resp_format)
					continue
				
				if len(tx.confirms) > 0 or tx.can_confirm or not tx.need_confirm or tx.need_send:
					send_response(client, "403 Forbidden", {"error": "cannot_confirm"}, resp_format)
					continue
				
				if peer.pubkey != tx.receiver_pubkey:
					send_response(client, "401 Unauthorized", {"error": "bad_rec_pubkey"}, resp_format)
					continue
				
				receiver_keys = PublicKey(tx.receiver_pubkey)
				
				try:
					data = libnacl.sign.Verifier(receiver_keys.hex_pk()).verify(content) # Verify
				except ValueError:
					send_response(client, "401 Unauthorized", {"error": "bad_signature"}, resp_format)
					continue
				
				try:
					data = self.keys.decrypt_seal(data) # Decrypt
				except libnacl.CryptError:
					send_response(client, "403 Forbidden", {"error": "bad_encryption"}, resp_format)
					continue
				
				if data[:32] != tx.out_seeds[1] or len(data) < 64:
					send_response(client, "401 Unauthorized", {"error": "bad_seed"}, resp_format)
					continue
				
				tx.confirms = data[64:]# TODO check size
				tx.out_seeds[2] = data[32:64]
				
				utils.logprint("Rec confirm "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
				tx.can_confirm = True
				
				resp["confirm_ok"] = tx.out_seeds[2]
				peer.up_in = True
			
			elif "idtysig" in url:
				doc = utils.verify_idty_sig(self.conf, content, self.conf["idty"]["pubkey"], self.keys.pubkey, utils.self_checktime)
				
				if not doc:
					send_response(client, "403 Forbidden", {"error": "bad_idty_sig"}, resp_format)
					continue
				
				self.conf = read_config(DIR, {
					"idty.sig": content.hex(),
					"idty.sigtime": doc["sigtime"]
				})
				utils.logprint("Updated idty sig: sigtime= {}".format(doc["sigtime"]), utils.LOG_INFO)
				
				self.timers["next_peer_info"] = time.time() + self.conf["server"]["peer_info_interval"]
				self.local_peer = utils.Peer.generate(self.conf, self.keys, self.peers)
				utils.logprint("Generated new peer info", utils.LOG_TRACE)
				self.client_th.spread_peer_info()
				
				resp["idtysig_ok"] = doc
			
			if "getconfirm" in url:
				sender_pubkey = utils.getargv("getconfirm", "", 1, url)
				try:
					in_seed1 = bytes.fromhex(utils.getargv("getconfirm", "", 2, url))
				except ValueError:
					send_response(client, "401 Unauthorized", {"error": "bad_url"}, resp_format)
					continue
				
				if not in_seed1 in self.tx_in_index:
					send_response(client, "404 Not Found", {"error": "unknown_tx"}, resp_format)
					continue
				
				tx = self.tx_in_index[in_seed1]
				
				if not tx.can_confirm or not tx.need_confirm or tx.need_send:
					send_response(client, "403 Forbidden", {"error": "cannot_confirm"}, resp_format)
					continue
				
				if sender_pubkey != tx.sender_pubkey:
					send_response(client, "401 Unauthorized", {"error": "bad_rec_pubkey"}, resp_format)
					continue
				
				message = tx.gen_mix_confirm(self.keys)
				resp["confirm"] = message
				tx.need_confirm = False
				tx.export_ubjson(self.db_txs)
				utils.logprint("Confirmed "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
			
			if "peers" in url:
				resp["peers"] = [{
					"raw": self.peers[peer].raw,
					"up_in": self.peers[peer].up_in,
					"up_out": self.peers[peer].up_out
				} for peer in self.peers]
			
			if "new" in url:
				try:
					new_peer = utils.Peer(self.conf, content)
				except Exception as e: # TODO more specific exceptions
					send_response(client, "400 Bad Request", {"error": "bad_peer"}, resp_format)
					utils.logprint("Peer: bad peer: "+str(e), utils.LOG_ERROR)
					continue
				
				if new_peer.sigtime + self.conf["server"]["peer_sig_age_max"] <= new_peer.rectime:
					send_response(client, "400 Bad Request", {"error": "too_old_sig"}, resp_format)
					utils.logprint("Peer: too old sig: "+new_peer.to_human_str(), utils.LOG_WARN)
					continue
				
				if new_peer.hash in self.peers and new_peer.sigtime <= self.peers[new_peer.hash].sigtime:
					send_response(client, "400 Bad Request", {"error": "more_recent_sig_exists"}, resp_format)
					utils.logprint("Peer: have more recent sig: "+new_peer.to_human_str(), utils.LOG_WARN)
					continue
				
				new_peer.up_in = True
				utils.logprint("Peer: "+new_peer.to_human_str(), utils.LOG_TRACE)
				self.peers[new_peer.hash] = new_peer
			
			if "info" in url:
				resp["info"] = self.local_peer.raw
			
			# Send response
			send_response(client, "200 OK", resp, resp_format)
		self.sock.close()
	
	def stop(self):
		self.work = False
		self.sock.shutdown(socket.SHUT_WR)

class ClientThread(Thread):
	def __init__(self, conf, peers, keys, local_peer, pool, tx_in_index, tx_out_index, db_txs, timers):
		Thread.__init__(self)
		
		self.conf = conf
		self.peers = peers
		self.keys = keys
		self.local_peer = local_peer
		self.pool = pool
		self.tx_in_index = tx_in_index
		self.tx_out_index = tx_out_index
		self.db_txs = db_txs
		self.timers = timers
		
		self.bma_endpoints = conf["client"]["bma_hosts"].copy()
		self.work = True
	
	def detect_peers(self):# Check known peers and ask them for their known peer list
		utils.logprint("Start peers detection", utils.LOG_TRACE)
		
		"""
		Try to interrogate 3 different random peers. When possible, do not interrogate 2 peers pertaining to the same identity.
		"""
		new_peers = {}
		left_peers = [self.peers[p] for p in self.peers] # peers we have not tested and pertaining to not tested identities
		random.shuffle(left_peers)
		left2_peers = left_peers.copy() # peers we have not tested
		answered = 0 # number of up tested peers
		while answered < 3 and len(left2_peers) > 0:
			
			# Choose a peer
			if len(left_peers) > 0:
				peer = left_peers.pop()
				left2_peers.remove(peer)
				to_remove = []
				for i in left_peers:
					if i.idty == peer.idty:
						to_remove.append(i)
				for i in to_remove:
					left_peers.remove(i)
			else:
				peer = left2_peers.pop()
			
			# Ask the chosen peer
			try:
				header, content = utils.sdata(peer.host, "GET", "/peers/info", proxy=self.conf["client"]["proxy"], proxy_onion_only=self.conf["client"]["proxy_onion_only"])
				peer.up_out = True
				
				try:
					data = ubjson.loadb(content)
					assert "peers" in data and type(data["peers"]) == list , "no peer list"
					assert "info" in data , "no peer info"
				except (ubjson.decoder.DecoderException, AssertionError) as e:
					utils.logprint("Peer detection: Encoding error: "+peer.to_human_str()+"\n\t"+str(e), utils.LOG_WARN)
					continue
				
				try:
					new_peer = utils.Peer(self.conf, data["info"])
				except Exception as e:
					print("Peer detection: info: "+str(e))
					continue
				new_peers.setdefault(new_peer.hash, [])
				new_peers[new_peer.hash].append(new_peer)
				
				for i_peer in data["peers"]:
					try:
						assert type(i_peer) == dict and "raw" in i_peer and "up_in" in i_peer and "up_out" in i_peer, "bad peer list encoding"
						new_peer = utils.Peer(self.conf, i_peer["raw"])
					except Exception as e:
						utils.logprint("Peer detection: "+str(e), utils.LOG_WARN)
					if new_peer.hash == self.local_peer.hash:
						continue
					new_peers.setdefault(new_peer.hash, [])
					new_peers[new_peer.hash].append(new_peer)
				
				answered += 1
			
			except (ConnectionRefusedError, socks.GeneralProxyError, socket.gaierror, socket.timeout):
				peer.up_out = False
				utils.logprint("Peer detection: Network error: "+peer.to_human_str(), utils.LOG_WARN)
		
		# Choose the more recent peer infos
		added_peers = False
		for peer in new_peers:
			new_peer = max(new_peers[peer], key=lambda p: p.sigtime) # select the more recent
			
			if new_peer.sigtime + self.conf["server"]["peer_sig_age_max"] <= new_peer.rectime or \
			   (peer in self.peers and new_peer.sigtime <= self.peers[peer].sigtime):
				utils.logprint("Peer detection: " + ("already have" if new_peer.sigtime == self.peers[peer].sigtime else "too old") + " sig: "+new_peer.to_human_str(), utils.LOG_TRACE)
				continue
			self.peers[peer] = new_peer
			added_peers = True
			utils.logprint("Peer: "+new_peer.to_human_str(), utils.LOG_TRACE)
		
		utils.logprint("Finished peers detection", utils.LOG_TRACE)
		
		# Remove old peers
		if added_peers:
			t = time.time() - self.conf["server"]["peer_sig_age_max"]
			to_remove = []
			for peer in self.peers:
				if self.peers[peer].sigtime < t:
					to_remove.append(peer)
			
			for peer in to_remove:
				utils.logprint("Remove old peer: "+self.peers[peer].to_human_str(), utils.LOG_TRACE)
				self.peers.pop(peer)
	
	def spread_peer_info(self):
		utils.logprint("Start spreading peer info", utils.LOG_TRACE)
		for peer in self.peers:
			try:
				utils.sdata(self.peers[peer].host, "POST", "/new", self.local_peer.raw, proxy=self.conf["client"]["proxy"], proxy_onion_only=self.conf["client"]["proxy_onion_only"])
				self.peers[peer].up_out = True
			except (ConnectionRefusedError, socks.GeneralProxyError, socket.gaierror, socket.timeout):
				self.peers[peer].up_out = False
				utils.logprint("Network error: "+self.peers[peer].to_human_str(), utils.LOG_WARN)
		utils.logprint("Finished spreading peer info", utils.LOG_TRACE)
	
	async def mix(self):
		can_mix = False
		for tx in self.pool:
			if not tx.need_send and not tx.need_confirm and not tx.tx_sent:
				can_mix = True
				break
		if not can_mix:
			return
			
		utils.logprint("Starting mix", utils.LOG_TRACE)
		t = time.time()
		client = None
		for bma_endpoint in self.bma_endpoints:
			client = Client(bma_endpoint)
			try:
				await client(bma.node.summary)
				break
			except:
				utils.logprint("BMA down: "+bma_endpoint, utils.LOG_WARN)
				await client.close()
				client = None
		if client:
			utils.logprint("BMA up: "+bma_endpoint, utils.LOG_TRACE)
			ready_txs = []
			
			# Get txs history
			history = await client(bma.tx.times, self.keys.pubkey, int(t-self.conf["mix"]["mix_req_age_max"]), int(t))
			await client.close()
			
			for tx in self.pool:
				if tx.tx_sent:
					continue
				if tx.need_send or tx.need_confirm:
					continue
				wanted_output = str(tx.in_amount) +":"+ str(tx.in_base) +":SIG("+ self.keys.pubkey +")"
				ok = False
				for in_tx in history["history"]["received"]:
					if (not "output" in in_tx and not "outputs" in in_tx) or not "comment" in in_tx:
						continue
					if in_tx["comment"] == utils.gen_comment(tx.in_seeds) \
					   and wanted_output in in_tx["output" if "output" in in_tx else "outputs"]:
						ok = True
						break
				if ok:
					ready_txs.append(tx)
			
			# Group txs by amount
			ready_txs_by_amount = {}
			for tx in ready_txs:
				if not tx.in_amount in ready_txs_by_amount:
					ready_txs_by_amount[tx.in_amount] = []
				ready_txs_by_amount[tx.in_amount].append(tx)
			
			# Check and send txs
			for amount in ready_txs_by_amount:
				txs = ready_txs_by_amount[amount]
				if len(txs) >= self.conf["mix"]["mix_min_txs"]:
					random.shuffle(txs)
					for tx in txs:
						try:
							#await utils.send_transaction(self.keys, tx.receiver_pubkey, tx.out_amount, utils.gen_comment(tx.out_seeds))
							utils.send_transaction(DIR+"/authfile", tx.receiver_pubkey, tx.out_amount, utils.gen_comment(tx.out_seeds))
							tx.tx_sent = True
							tx.export_ubjson(self.db_txs)
						except socket.timeout:
							utils.logprint("Error when sending tx: timeout", utils.LOG_ERROR)
						except Exception as e:
							utils.logprint("Error when sending tx: " + str(e), utils.LOG_ERROR)
				else:
					utils.logprint("Not enough ready txs to mix ("+str(amount)+"u) (only "+str(len(txs))+")", utils.LOG_TRACE)
			
			utils.logprint("Mix finished", utils.LOG_TRACE)
		else:
			utils.logprint("All BMA endpoints down: cannot mix!", utils.LOG_ERROR)
	
	def run(self):
		loop = asyncio.new_event_loop()
		nest_asyncio.apply(loop)
		loop.run_until_complete(self.start_client())
	
	async def start_client(self):
		t = time.time()
		next_mix = t + self.conf["mix"]["mix_interval"]
		next_peers_detection = t + self.conf["server"]["peer_detect_interval"]
		self.timers["next_peer_info"] = t + self.conf["server"]["peer_info_interval"]
		
		self.detect_peers()
		local_peer = utils.Peer.generate(self.conf, self.keys, self.peers)
		self.spread_peer_info()
		
		while self.work:
			t = time.time()
			
			# Detect peers
			if t > next_peers_detection:
				self.detect_peers()
				next_peers_detection = time.time() + self.conf["server"]["peer_detect_interval"]
			
			# Mix
			if t > next_mix:
				await self.mix()
				next_mix = time.time() + self.conf["mix"]["mix_interval"]
			
			for tx in self.pool:
				
				if tx.need_send:
					utils.logprint("Send "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
					
					# Find all the peers with that pubkey
					peers = []
					for peer in self.peers:
						if self.peers[peer].pubkey == tx.receiver_pubkey:
							peers.append(self.peers[peer])
					random.shuffle(peers)
					
					for peer in peers:
						message = self.keys.sign(tx.out_seeds[1] + tx.message) # Sign
						
						try:
							header, content = utils.sdata(peer.host, "POST", "/mix/"+base64.urlsafe_b64encode(self.local_peer.hash).decode()+"/"+str(tx.out_amount)+"/"+str(tx.out_base), message, proxy=self.conf["client"]["proxy"], proxy_onion_only=self.conf["client"]["proxy_onion_only"])
							peer.up_out = True
							data = ubjson.loadb(content)
							assert data["mix_ok"] == tx.out_seeds[1]
						except (ConnectionRefusedError, socks.GeneralProxyError, socket.gaierror, socket.timeout):
							peer.up_out = False
							utils.logprint("Network error: "+peer.to_human_str(), utils.LOG_WARN)
							continue
						except (ubjson.decoder.DecoderException, KeyError, AssertionError):
							utils.logprint("Bad response: "+peer.to_human_str(), utils.LOG_WARN)
							continue
						
						tx.need_send = False
						tx.receiver_hash = peer.hash
						tx.export_ubjson(self.db_txs)
						utils.logprint("Sent "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
						break
					
					if len(peers) == 0:
						utils.logprint("No peer for: "+tx.receiver_pubkey, utils.LOG_WARN)
				
				elif tx.can_confirm and tx.need_confirm and tx.send_confirm:
					utils.logprint("Confirm "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
					
					if tx.sender_hash in self.peers:
						peer = self.peers[tx.sender_hash]
						message = tx.gen_mix_confirm(self.keys)
						
						try:
							header, content = utils.sdata(peer.host, "POST", "/confirm/"+base64.urlsafe_b64encode(self.local_peer.hash).decode()+"/"+tx.in_seeds[1].hex(), message, proxy=self.conf["client"]["proxy"], proxy_onion_only=self.conf["client"]["proxy_onion_only"])
							data = ubjson.loadb(content)
							peer.up_out = True
							assert data["confirm_ok"] == tx.in_seeds[2]
							tx.need_confirm = False
							tx.export_ubjson(self.db_txs)
							utils.logprint("Confirmed "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
						except (ConnectionRefusedError, socks.GeneralProxyError, socket.gaierror, socket.timeout):
							peer.up_out = False
						except (ubjson.decoder.DecoderException, KeyError, AssertionError):
							utils.logprint("Bad response: "+peer.to_human_str(), utils.LOG_WARN)
					else:
						utils.logprint("No peer for: "+tx.receiver_pubkey, utils.LOG_WARN)
			
			# Generate peer info
			if t > self.timers["next_peer_info"]:
				self.timers["next_peer_info"] = time.time() + self.conf["server"]["peer_info_interval"]
				self.local_peer = utils.Peer.generate(self.conf, self.keys, self.peers)
				utils.logprint("Generated new peer info", utils.LOG_TRACE)
				self.spread_peer_info()
			
			# Remove expired requests
			expire_txs = []
			expire_t = t - self.conf["mix"]["mix_req_age_max"]
			for tx in self.pool:
				if tx.date < expire_t:
					expire_txs.append(tx)
			for tx in expire_txs:
				self.tx_in_index.pop(tx.in_seeds[1])
				self.tx_out_index.pop(tx.out_seeds[1])
				self.db_txs.delete(tx.in_seeds[2])
				self.pool.remove(tx)
			if len(expire_txs) > 0:
				utils.logprint("Removed "+str(len(expire_txs))+" expired txs", utils.LOG_TRACE)
			
			time.sleep(4)

def get_credentials(conf):
	salt = conf["crypto"]["id_salt"]
	if salt == "":
		salt = getpass.getpass("Enter your passphrase (salt): ")
	password = conf["crypto"]["id_password"]
	if password == "":
		password = getpass.getpass("Enter your password: ")
	return salt, password

# Main function
def main():
	# Load conf
	conf = read_config(DIR)
	
	# Load peers
	peers = {}
	db_peers = plyvel.DB(DIR+"/db_peers", create_if_missing=True)
	utils.load_peers(conf, db_peers, peers)
	utils.logprint("Loaded " + str(len(peers)) + " peers", utils.LOG_INFO)
	
	# Load txs
	pool = []
	tx_in_index = {}
	tx_out_index = {}
	db_txs = plyvel.DB(DIR+"/db_txs", create_if_missing=True)
	load_txs(db_txs, pool, tx_in_index, tx_out_index)
	
	# Get private key
	salt, password = get_credentials(conf)
	keys = SigningKey.from_credentials(salt, password)
	utils.logprint("Pubkey: "+keys.pubkey, utils.LOG_INFO)
	
	# Generate authfile
	keys.save_seedhex_file(DIR+"/authfile")
	
	# Generate peer info
	local_peer = utils.Peer.generate(conf, keys, {})
	
	timers = {"next_peer_info": 0}
	
	# Start threads
	clientThread = ClientThread(conf, peers, keys, local_peer, pool, tx_in_index, tx_out_index, db_txs, timers)
	serverThread = ServerThread(conf, peers, keys, local_peer, pool, tx_in_index, tx_out_index, db_txs, timers, clientThread)
	
	clientThread.start()
	serverThread.start()
	
	# Wait
	try:
		while True:
			input()
	except KeyboardInterrupt:
		utils.logprint("Stopping (^C)...", utils.LOG_INFO)
	
	# Stop threads
	serverThread.stop()
	clientThread.work = False
	serverThread.join()
	clientThread.join()
	
	# Wipe authfile
	f = open(DIR+"/authfile", "w")
	f.write(secrets.token_hex(32))
	f.close()
	os.remove(DIR+"/authfile")
	
	# Save
	utils.save_peers(db_peers, peers)
	db_peers.close()
	save_txs(db_txs, pool)
	db_txs.close()

if __name__ == "__main__":
	
	if "-v" in sys.argv:
		utils.VERBOSITY |= utils.LOG_TRACE
	
	DIR = os.path.expanduser(utils.getargv("-d", DIR))
	if DIR != "" and DIR[len(DIR)-1] == "/":
		DIR = DIR[:len(DIR)-1] # Remove last slash
	os.makedirs(DIR, exist_ok=True)
	
	conf_overwrite = {}
	if "-P" in sys.argv:
		import subprocess
		utils.logprint("Fetching public address...", LOG_INFO)
		PUBLIC_HOST = subprocess.run(['curl', '-4', 'https://zettascript.org/tux/ip/'], stdout=subprocess.PIPE).stdout.decode("utf-8")
		utils.logprint("Public host: " + PUBLIC_HOST, LOG_INFO)
		conf_overwrite["server.public_host"] = PUBLIC_HOST
		read_config(DIR, conf_overwrite)
	
	if "-s" in sys.argv:
		main()
	
	elif "-i" in sys.argv or "-I" in sys.argv:
		conf_overwrite = {}
		if "-I" in sys.argv:
			conf_overwrite["crypto.id_salt"], conf_overwrite["crypto.id_password"] = utils.gen_keys()
		conf = read_config(DIR, conf_overwrite)
		
		salt, password = get_credentials(conf)
		keys = SigningKey.from_credentials(salt, password)
		
		if conf["crypto"]["id_salt"] == "":
			conf_overwrite["crypto.id_salt"] = salt
		if conf["crypto"]["id_password"] == "":
			conf_overwrite["crypto.id_password"] = password
		
		if "-g" in sys.argv:
			loop = True
			while loop:
				idty_keys = SigningKey.from_credentials(
					getpass.getpass("Identity passphrase (salt):"),
					getpass.getpass("Identity password:")
				)
				print(idty_keys.pubkey)
				loop = input("Is that the right pubkey? [yn]: ").lower() != "y"
			sig, doc = utils.gen_idty_sig(keys, idty_keys)
			conf_overwrite["idty.sig"] = sig.hex()
			conf_overwrite["idty.pubkey"] = idty_keys.pubkey
			conf_overwrite["idty.sigtime"] = doc["sigtime"]
		
		conf = read_config(DIR, conf_overwrite)
	
	elif "-p" in sys.argv:
		peer_file = open(os.path.expanduser(utils.getargv("-p", "peers.ubj")), "rb")
		new_peers = ubjson.loadb(peer_file.read())
		peer_file.close()
		
		# Load conf
		conf = read_config(DIR)
		
		# Load peers
		peers = {}
		db_peers = plyvel.DB(DIR+"/db_peers", create_if_missing=True)
		utils.load_peers(conf, db_peers, peers)
		
		# Import peers
		for raw in new_peers:
			try:
				new_peer = utils.Peer(conf, raw)
			except Exception as e: # TODO more specific exceptions
				print("Error: invalid peer data: "+str(e))
				continue
			if new_peer.hash in peers and peers[new_peer.hash].sigtime > new_peer.sigtime:
				print("Too old: "+new_peer.to_human_str())
				continue
			peers[new_peer.hash] = new_peer
			print("Peer: "+new_peer.to_human_str())
		
		# Save
		utils.save_peers(db_peers, peers)
		db_peers.close()
	
	elif "-e" in sys.argv:
		# Load conf
		conf = read_config(DIR)
	
		# Get private key
		salt, password = get_credentials(conf)
		keys = SigningKey.from_credentials(salt, password)
		print("Pubkey: "+keys.pubkey)
		
		# Generate peer info
		local_peer = utils.Peer.generate(conf, keys, {})
		if local_peer == None:
			exit()
		
		peer_file = open(os.path.expanduser(utils.getargv("-e", "peer_info.ubj")), "wb")
		peer_file.write(ubjson.dumpb([local_peer.raw]))
		peer_file.close()
	
	elif "-k" in sys.argv:
		conf = read_config(DIR)
		salt, password = get_credentials(conf)
		keys = SigningKey.from_credentials(salt, password)
		print(keys.pubkey)
	
	elif "-V" in sys.argv or "--version" in sys.argv:
		print(utils.VERSION)
	
	elif "--help" in sys.argv:
		print("\
ĞMixer-py Server "+utils.VERSION+"\n\
\n\
Options:\n\
 -s         Start server\n\
 -i         Init config\n\
 -I         Init config (generate random keys)\n\
 -p <path>  Import peers from file\n\
 -e <path>  Export peer info to file (output is compatible with -p)\n\
 -k         Display public key\n\
 -V         Display version\n\
 --version\n\
 --help     Display help\n\
\n\
 -d <path>  Change config & data dir\n\
  default: ~/.config/gmixer\n\
 -v         Verbose\n\
 -P         Auto set public address (overwrites config)\n\
 -g         (with -i or -I only) Generate identity signature\n\
")
